set terminal gif
set output "plots/allocf_r2.gif"
set xlabel "Clock cycles"
set ylabel "Nr of families allocated"
set title "Family allocations"
set format x "%1.2g"
set xrange [0:2500000]
set tics out
plot "results/threads_r2.txt" using 1:5 smooth csplines title "Families currently allocated" lc rgb "blue"
