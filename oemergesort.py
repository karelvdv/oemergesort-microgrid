def round2(n):
	"""
	Round n to the smallest power of two >= n.
	"""
	k = 1
	while k < n:
		k = k * 2
	return k

def log2(n):
	k = 0
	while n > 1:
		n = n / 2
		k = k + 1
	return k


def compex(a, i, j):
	"""
	Compare-exchange a[i] and a[j]
	"""
	global compares
	compares+=1
	if a[i] > a[j]:
		a[i], a[j] = a[j], a[i]

def oemerge(a, lo, hi, n, step):
	"""
	Recursive odd-even merge.
	"""
	if n == 2:
		compex(a, lo, lo + step)
	elif n > 1:
		m = n / 2
		step2 = step * 2
		oemerge(a, lo, hi, n - m, step2)
		oemerge(a, lo + step, hi, m, step2)
		for i in xrange(lo + step, hi - step, step2):
			compex(a, i, i + step)

def oemergesort(a, lo, hi, n):
	"""
	Recursive odd-even merge sort.
	"""
	if n > 1:
		m = round2(n) / 2
		oemergesort(a, lo, lo + m, m)
		oemergesort(a, lo + m, hi, n - m)
		oemerge(a, lo, hi, n, 1)

def oemerge_bottomup_p2(a, lo, hi, n):
	"""
	Bottom up odd-even merge for n=power of two.
	"""
	half = n / 2
	step = half
	while step:
		for j in xrange(step % half, n - step, step * 2):
			m = min(lo + j + step, hi - step)
			for i in xrange(lo + j, m):
				compex(a, i, i + step)
		step = step / 2

def oemerge_bottomup_p2_2(a, lo, hi, n):
	"""
	Bottom up odd-even merge for n=power of two, with only one inner loop.
	"""
	half = n / 2
	step = half
	while step:
		step2 = step * 2
		nchunks = (n - step % half) / step2
		start = lo + (step % half)
		for i in xrange(nchunks * step):
			chunk = i / step
			idx   = i % step
			loc   = start + chunk * step2 + idx
			compex(a, loc, loc + step)
		step = step / 2

def oemerge_bottomup(a, lo, hi, stride):
	"""
	Bottom up odd-even merge.
	"""
	half = round2(stride) / 2
	step = half
	while step:
		step2   = step * 2
		start   = lo + (step % half)
		range   = hi - step - start
		nchunks = range / step2
		nleft   = min(step, range % step2)
		for i in xrange(0, nchunks * step + nleft):
			chunk = i / step
			idx   = i % step
			loc   = start + chunk * step2 + idx
			compex(a, loc, loc + step)
		step = step / 2

def oemergesort_bottomup_inner(a, n, stride):
	"""
	The merge sort inner loop. Stride must be (and is guaranteed to be) a power of two.
	"""
	half = stride / 2
	step = half
	while step:
		step2   = step * 2
		offset  = step % half
		nchunks = (stride - offset) / step2
		for lo in xrange(0, n - stride + 1, stride):
			start = lo + offset
			for i in xrange(nchunks * step):
				chunk = i / step
				idx   = i % step
				loc   = start + idx + chunk * step2
				compex(a, loc, loc + step)
		step = step / 2

def oemergesort_bottomup(a):
	"""
	Bottom up odd-even merge sort
	"""
	n = len(a)
	stride = 2
	while stride < n:
		#for i in xrange(0, n - stride + 1, stride):
		#	oemerge_bottomup(a, i, i + stride, stride)
		oemergesort_bottomup_inner(a, n, stride)
		rest = n % stride
		if rest & (rest - 1):
			oemerge_bottomup(a, n - rest, n, stride)
		stride = stride * 2
	oemerge_bottomup(a, 0, n, n)

def verify(a):
	for i in xrange(1, len(a)):
		if a[i - 1] > a[i]:
			return False
	return True

def ncompares(n):
	"""
	Computes the total number of compex operations.
	"""
	from math import log
	def lg(n):
		return log(n) / log(2)
	# Merge function
	def T1(n):
		if   n == 1: return 0
		elif n == 2: return 1
		else: return 2*T(n/2) + n/2 - 1
	def T(n):
		if   n == 1: return 0
		elif n == 2: return 1
		else: return T1(n/2) + T(n-n/2) + n/2 - 1
	# Merge sort function
	def S1(n):
		if   n == 1: return 0
		elif n == 2: return 1
		else: return 2*S(n/2) + T(n)
	def S(n):
		if   n == 1: return 0
		elif n == 2: return 1
		else: return S1(round2(n)/2) + S(n-round2(n)/2) + T(n)
	return S(n)

if __name__ == '__main__':
	from random import shuffle
	from time import clock
	global compares
	
	#for y in xrange(1024):
	y=10240
	a = range(y)
	shuffle(a)
	compares=0
	t0=clock()
	oemergesort_bottomup(a)
	t1=clock()-t0
	print y, verify(a), compares, ncompares(y), t1