ttotal = select('*.threads.totalalloc')
tcur   = select('*.threads.curalloc')
ftotal = select('*.families.totalalloc')
fcur   = select('*.families.curalloc')
iprev  = zerorec
for i in input:
	tt = sum(i[x] - iprev[x] for x in ttotal)
	tc = sum(i[x] for x in tcur)
	ft = sum(i[x] - iprev[x] for x in ftotal)
	fc = sum(i[x] for x in fcur)
	dt = i[kernel.cycle] - iprev[kernel.cycle]
	# cycle, avg, cur, avg, cur
	tabulate(i[kernel.cycle], tt / dt, tc, ft / dt, fc)
	iprev = i
