def oemerge_bottomup(a, lo, hi, stride):
	half = round2(stride) / 2
	step = half
	while step:
		step2   = step * 2
		start   = lo + (step % half)
		range   = hi - step - start
		nchunks = range / step2
		nleft   = min(step, range % step2)
		# sl_create(, some place, ...)
		for i in xrange(nchunks * step + nleft):
			chunk = i / step
			idx   = i % step
			loc   = start + chunk * step2 + idx
			compex(a, loc, loc + step)
		step = step / 2

def oemergesort_bottomup_inner(a, n, stride):
	half = stride / 2
	step = half
	while step:
		step2   = step * 2
		offset  = step % half
		nchunks = (stride - offset) / step2
		# sl_create(, some place, ...)
		for lo in xrange(0, n - stride + 1, stride):
			start = lo + offset
			# sl_create(, PLACE_LOCAL, ...)
			for i in xrange(nchunks * step):
				chunk = i / step
				idx   = i % step
				loc   = start + idx + chunk * step2
				compex(a, loc, loc + step)
		step = step / 2

def oemergesort_bottomup(a, n):
	stride = 2
	while stride < n:
		oemergesort_bottomup_inner(a, n, stride)
		rest = n % stride
		# if rest is not zero or a power-of-two...
		if rest & (rest - 1):
			oemerge_bottomup(a, n - rest, n, stride)
		stride = stride * 2
	oemerge_bottomup(a, 0, n, n)