iprev  = zerorec
stprev = 0
sel    = select('*.execute.op')
effw   = window(100)
r      = mgconf.masterfreq / mgconf.corefreq
for i in input:
	ops         = sum(i[x] - iprev[x] for x in sel)
	activecores = sum(1 for x in sel if i[x] - iprev[x])
	st          = i[kernel.cycle] / r
	slots       = activecores * (st - stprev)
	if slots:
		pl_eff  = ops / float(slots)
		effw.append(pl_eff)
		tabulate(i[kernel.cycle], pl_eff, sum(effw) / len(effw))
		iprev  = i
		stprev = st
