\documentclass[11pt]{paper}
\usepackage[a4paper]{geometry}
\usepackage{graphicx, url,float}
\usepackage{listings, framed}
\usepackage[numbers]{natbib}
\usepackage{url}
\title{The performance of odd-even merge sort on MicroGrid}
\author{Karel van der Veldt (5943086)\\{\small May 27, 2011}}
\begin{document}
\maketitle

\abstract
This is my report on the Concurrent Systems sorting assignment, for which I investigated the performance of the odd-even merge sort algorithm. The contents of the report is as follows: first a description of the algorithm and how I modified it, then performance analysis, and finally conclusions.

\section{Odd-even merge sort}
Odd-even merge sort is a \emph{sorting network} invented by Ken Batcher\cite{batcher}. A sorting network is an abstract model used to sort a sequence of numbers, with the property that the sequence of comparisons is set in advance and are independent, meaning they can be executed in parallel. The worst and best case time complexity is $O(N\log_2N)$ (where $N$ is the length of the array to be sorted). Unlike normal merge sort, odd-even merge sort is an \emph{in-place} algorithm, and the space complexity is $O(1)$.

\subsection{The algorithm}
Like normal merge sort, odd-even merge sort recursively divides the input array into two halves, which are sorted in parallel and then merged. The difference is in the merge function. The merge function takes as input an array, which is further recursively divided into two interleaved subarrays: one for all elements on the even indices, and one for all elements on the odd indices. The interleaved arrays never overlap (access the same indices), meaning they can be sorted in parallel. Afterwards each even and odd pair from the two interleaved subarrays are sorted. The pairs never overlap, so they can be sorted in parallel. This sorts the array that was input to the merge function. Note that the length of the even and odd subarrays is halved with each recursion, and the recursion stops when the length is two. The fact that this algorithm works is proven by the zero-one principle \cite{zeroone}.

\subsection{The modified algorithm}
There is one big problem with this algorithm in regards to this assignment: it is only defined for arrays where the length is a power of two. Since we have to sort arrays of length 10240, that is not very useful. Although we could find \emph{no} information about generalizing for arbitrary array lengths in papers or on websites, we eventually figured it out ourselves. We will show why the algorithm incorrectly sorts arrays of non power-of-two length, and the modifications required to solve this problem.
\\\\
Consider an array $A$ with length $N=6$, which will be divided into two subarrays of length 3: $a=(a_0, a_1, a_2)$ and $b=(b_0, b_1, b_2)$. $A=(a_0, a_1, a_2, b_0, b_1, b_2)$.
\\\\
First, the lengths of even and odd interleaved arrays are not equal: the even subarrays $(a_0, a_2)$ and $(b_0, b_2)$ have a length of 2, and the odd arrays $(a_1)$ and $(b_1)$ have a length of 1. In the original algorithm an array of length 1 can never occur (and would have caused an infinite loop if it did), but of course such an array is already sorted, so we modify the algorithm to simply do nothing in that case.
\\\\
The second problem is that when subarrays $a$ and $b$ are merged, the even and odd interleaved arrays will have elements $a'=(a_0, a_2, b_1)$ and $b'=(a_1, b_0, b_2)$ respectively, which means that some elements in the interleaved arrays will contain elements that have an opposite parity (even or odd) index! For example, element $b_1$ lies at an \emph{odd} index in the $b$ subarray, but is an element of the \emph{even} interleaved array $a'$.
\\\\
To solve this problem, we introduce the function $round2(n)$, which rounds $n$ to the smallest power-of-two $>= n$. So, if $n$ is already a power-of-two, then $round2(n)=n$. The algorithm is modified so that, instead of taking $N/2$ for the subarray lengths, we take $round2(N)/2$ for subarray $a$, and $N-round2(N)/2$ for subarray $b$. If $N$ is already a power-of-two, the algorithm is unchanged.
\\\\
So, going back to the example, we now have two subarrays $a=(a_0, a_1, a_2, a_3)$ and $b=(b_0, b_1)$ (and $A=(a_0, a_1, a_2, a_3, b_0, b_1)$). The interleaved arrays are now $a'=(a_0, a_2, b_0)$ and $b'=(a_1, a_3, b_1)$, which now only contain elements of the correct parity. Note that, since $|a|$ is always a power-of-two, it will be sorted correctly according to the original algorithm. Array $b$ may not be a power-of-two, but it will be recursively divided into further subarrays that do have a power-of-two lengths until one or both halves have a length of 2 or 1. To merge subarrays $a$ and $b$, we only need to take into account that the lengths of the even and odd interleaved arrays are not necessarily equal: the even subarray always has a length of $N-floor(N/2)$, and the odd subarray always has a length of $floor(N/2)$. Note that if $N$ is a power-of-two, the lengths of the subarrays are always equal, as in the original algorithm.
\\\\
These are all the modifications required to sort arrays of arbitrary lengths using odd-even merge sort. It is important to note that, if the array length is a power-of-two, the algorithm remains \emph{unchanged} from the original. Listing \ref{lst:rec} shows the complete pseudo-code. To show the difference between the original algorithm and the modified algorithm, lines that are modified are prefixed with a minus sign, and the code they are replaced by are listed right below and prefixed with a plus sign. In fact, we only need to change 4 lines and add the $round2$ function.

\subsection{Problems with recursion}
Recursive odd-even merge sort is actually not very suitable for MicroGrid, as we discovered when we implemented it.
\\\\
To sort an array of $N$ elements, the divide phase ($oemergesort$ function in Listing \ref{lst:rec}) creates a large (binary) concurrency tree. The total number of families (nodes) in the tree is $N-1$ (not $2^{\log_2(N)+1}-1$ as for a normal binary tree, because the last level is skipped), and the total number of threads (branches) is $N-2$. The entire tree must be created \emph{before} any sorting can take place, which limits the number of resources left to do the actual sorting. In addition, none of those families can be sequentialized. This is because each family by itself only takes up a small amount of resources of two threads each, which block on the recursive create/sync that they execute. To avoid deadlock, there must be enough resources to reach the lowest level of the tree. In practice this means the place the program is allocated to must have a minimum amount of free resources available, or the program deadlocks. There is also additional overhead from having to synchronize so many families.
\\\\
For example, to sort an array of 10240 elements, 10238 threads are needed, just for the concurrency tree. It does not matter that 10240 is not a power of two. On a 32 core place, there are $32 \times 256 = 8192$ thread table entries, so the program will deadlock. In practice, a minimum of 64 cores is required to sort an array of 10240 elements. Clearly this is not scalable.
\\\\
Finally, a similar problem occurs in the merge phase ($oemerge$ function in Listing \ref{lst:rec}), which is also a recursive function. To execute our program without deadlocks on a 64 core place, we needed to force the for-loop in Listing \ref{lst:rec} to execute sequentially with \emph{sl\_\_forceseq}, which is obviously not desirable. Even then, the program could not run reliably: sometimes it would finish correctly and other times it would deadlock.
\\\\
In short, recursive odd-even merge sort makes inefficient use of the available resources, is prone to deadlocks, and is unable to expose all the concurrency inherent in the algorithm. It may be possible to overcome these problems by limiting the concurrency, but that is exactly what we do not want. We want to use as much concurrency as possible, otherwise what is the point? We decided to take a different approach, which is described in the next section.

\subsection{Iterative odd-even merge sort}
The solution to the problems described in the previous section is an iterative odd-even merge sort, that sorts the array from the bottom up, instead of top down as in the recursive implementation. This algorithm was given in chapter 11 of Algorithms in Java \cite{javaalg} without explanation, though again only for power-of-two arrays. Regardless, we figured out how to modify it to sort arrays of arbitrary lengths. Our algorithm is given in Listing \ref{lst:iter}, and is explained in more detail below. The advantage of the iterative algorithm is that the concurrency tree has a fixed depth and families are larger, meaning they can be sequentialized if necessary. What actually happens is that MicroGrid is given much more information about the concurrency inherent in the algorithm, compared to the recursive algorithm. This lets MicroGrid schedule threads and manage resources much more effectively.
\\\\
The iterative algorithm divides the whole array into subarrays of power-of-two lengths, starting from 2 to the largest power-of-two smaller than $N$ (the length of the total array), called the \emph{stride}. Unlike the recursive implementation this is done in a single, large family. Each subarray is then merged concurrently in \emph{steps}, from $stride/2$ down to $1$ in powers-of-two. The subarrays are divided into non-overlapping \emph{chunks} of length $2 \times step$. For example, take a subarray $a=(a_0, ..., a_7)$ and a step of 4. It has one chunk (of length 8), in which the following 4 compare-exchanges will be performed: $a_0:a_4$, $a_1:a_5$, $a_2:a_6$, $a_3:a_7$. Note that none of those pairs overlap, and can thus be executed in parallel (within the same family).
\\\\
Since the total array may not be evenly divisible by the stride, the rest is merged separately. The rest only has to be merged if $|rest|$ is neither 0 or a power-of-two, because otherwise it was already sorted in a previous iteration. In this merge we have to take into account that it might contain a chunk that is smaller than $2 \times step$. For example, a rest of length 7 contains one chunk of length 8, but only 3 out of 4 compare-exchanges are performed: $a_0:a_4$, $a_1:a_5$, $a_2:a_6$.
\\\\
Finally, the whole array is merged once, using the same algorithm used for merging the rest, and the output is the sorted array.
\\\\
Listing \ref{lst:iter} shows a Python implementation of the algorithm, with comments to show how the loops are parallelized on MicroGrid (more on that in the next section). Note that this implementation exposes \emph{all} concurrency explicitely in a straightforward manner (for-loops), unlike the recursive algorithm. Should the architecture run out of resources, it can sequentialize the families without running into deadlocks. In practice, the algorithm can sort an array of 10240 elements even if only a single core is available, whereas the recursive algorithm required at least 64 cores. This implies that the algorithm scales with the number of resources available, since it is possible to use as many or as few resources as desired. Of course, optimal efficiency is only achieved using certain parameters, which are discussed in the next section.
\\\\
Side note: In the actual implementation all multiplications, divisions and modulo operations are replaced by bitwise operations, and the $round2$ function is rewritten more efficiently.

\pagebreak
\begin{framed}
\lstset{language=Python, tabsize=4, numbers=left, basicstyle=\footnotesize, captionpos=b, caption={Recursive odd-even merge sort for any N}, label=lst:rec}
\lstinputlisting{rec.py}
\end{framed}

\pagebreak
\begin{framed}
\lstset{language=Python, tabsize=4, numbers=left, basicstyle=\footnotesize, captionpos=b, caption={Iterative odd-even merge sort for any N}, label=lst:iter}
\lstinputlisting{iter.py}
\end{framed}


\section{Performance analysis}
In this section we present the performance analysis of our iterative odd-even merge sort implementation. All analysis was done on the ML COMA 256-core, 4 DDR channel configuration.

\subsection{Execution time and speedup}
Before looking deeper into the performance of the algorithm, we want to know with which parameters the data files are sorted the quickest. We measured the execution time for sorting each of the given data files, while varying the number of places, number of cores per place, and family block size. We only measured the actual execution time of the algorithm, and not the startup time of the simulator or anything else. We found that setting specific block sizes decreased the execution time, so we left it unspecified to let the architecture decide. The results are given in Table \ref{tab:time}. The shortest (best) execution times are bolded. In some cases the result was not sorted, these are marked with a $^*$. It is not clear why the data is not sorted in some cases, but it is not a bug in the program (because then all results would be incorrect). It looks like it may be an issue with memory coherency, or possibly a bug in the simulator. We cannot show results of the recursive algorithm, because most of the time it would deadlock. However, when it did finish the execution time was slightly worse than the sequential implementation.
\\\\
From the results it is clear that a single 32-core place gives the best performance for sorting an array of 10240 integers, unless the data was already sorted (the data files c1, cm, cz, inc and topinc are already sorted). Since it gives the best results, we assume a single 32-core place for all further analysis.
\\\\
The leftmost column shows the execution time of a sequential implementation of the algorithm, written in straightforward C, which runs in a single thread. The SL implementation gives a speedup of 10x or more over the sequential implementation, so that is not bad, but considering there are so many cores available an even better speedup might be expected.
\begin{table}%[h!]
\begin{center}
\begin{tabular}{l || c || c | r | r | r | r | r | r}
 &Sequen-& \multicolumn{7}{c}{Places x cores execution time ($\mu$s)}\\
Data file & tial ($\mu$s) & 1x1 & 1x16 & 1x32 & 1x64 & 2x16 & 2x32 & 2x64\\\hline
c1 & 24680 & 12216 & 1810 & 1647 & {\bf 1599} & 1795 & 1715 & 1702\\
cm & 24680 & 12216 & 1810 & 1647 & {\bf 1599} & 1795 & 1715 & 1702\\
cz & 24680 & 12216 & 1810 & 1647 & {\bf 1599} & 1795 & 1715 & 1702\\
dec & 24464 & 12755 & 1918 & {\bf 1773} & 1779 & 1922 & 1874 & $^*$1913\\
inc & 24680 & 12216 & 1810 & 1647 & {\bf 1599} & 1795 & 1715 & 1702\\
r1 & 23947 & 13434 & 2204 & {\bf 2138} & 2310 & 2282 & 2319 & $^*$2471\\
r2 & 23945 & 13374 & 2200 & {\bf 2140} & 2304 & 2284 & 2319 & 2477\\
ro1\_3l & 24283 & 13257 & 2011 & {\bf 1930} & 2012 & 2067 & 2063 & 2136\\
ro1l & 24338 & 13279 & 2007 & {\bf 1930} & 1980 & 2065 & 2038 & 2076\\
ro3\_5l\_1\_7r & 23997 & 13492 & 2161 & {\bf 2093} & 2259 & 2240 & 2275 & $^*$2412\\
topdec & 24464 & 12755 & 1918 & {\bf 1773} & 1779 & 1922 & 1874 & $^*$1913\\
topinc & 24680 & 12216 & 1810 & 1647 & {\bf 1599} & 1795 & 1715 & 1702\\
ur1 & 23951 & 13388 & 2203 & {\bf 2135} & 2304 & 2278 & 2315 & 2465\\
ur2 & 23936 & 13357 & 2205 & {\bf 2136} & 2314 & 2291 & 2325 & 2477\\
\end{tabular}
\end{center}
\caption{The execution time to sort the data files with iterative odd-even merge sort. The fastest times are bolded, and the results designated with $^*$ are incorrect: the array was not sorted, possibly because of memory coherency issues or a bug in the simulator. The first column shows the execution time of the sequential implementation (not using any threads).}
\label{tab:time}
\end{table}

\subsection{Cache coherency}
Odd-even merge sort is a data independent algorithm, meaning it always performs the same steps for a given problem size regardless of the actual data. Why then, is the execution time not the same for all data files? To sort an array of $N$ elements, odd-even merge sort requires $N_{compex}=N\log_2(N)(\log_2(N)-1)/4+n-1$ compare-exchange (compex) operations \cite{batcher}. A compex executes two load words, one branch, and two store words only if the branch is taken (to swap the data, see Listing \ref{lst:rec}). This means that, if two values are already sorted they will not be swapped, and no store words will be executed. The difference in execution time is thus caused by the need to maintain memory coherency, since memory writes invalidate the cache. Figure \ref{fig:dcachetotal} shows the total number of L1 D-cache hits/misses over time for sorting data file c1.d and r2.d. The former data file is already sorted, so no memory writes are ever executed. Since the cache is never invalidated, the coherency protocol never needs to kick in. In contrast, the r2.d data file takes the longest time to sort, meaning it must have the most random data distribution of all data files. Accordingly, more memory writes are performed, which causes the coherency protocol to kick in more often, and that affects the total execution time. Figure \ref{fig:dcachetotal} indeed shows more cache misses and fewer cache hits for r2.d, compared to c1.d.

\subsection{Memory latency and bandwidth}
%We want to know the memory bandwidth requirements for sorting 10240 64-bit integers, and whether it is scalable.
The configuration uses DDR3-2400 at 1200MHz with 4 channels, meaning there is a total of 48 GB/s of bandwidth divided over all cores. We do not know the CAS latency, but assuming it is 10 (reasonable assumption), the access latency is 8.3ns per word (8 bytes). Sorting an array of $N$ elements requires $N_{compex}$ compex operations, meaning at least $2N_{compex}$ load words and at most an additional $2N_{compex}$ store words.
\\\\
To sort an array of 10240 elements requires exactly $N_{compex}=448511$ compex operations, so the minimum memory access latency is 7.45ms (only loads) and the maximum is 14.90ms (loads and stores). However, MicroGrid should be able to hide most or all of this latency. The memory coherency protocol has more effect on execution time.
\\\\
We also want to know the memory bandwidth requirements for sorting an array of 10240 elements. The algorithm requires at least $16N_{compex}=7176176$ bytes read, and at most an additional $16N_{compex}$ bytes written. Given the execution time for sorting an already sorted array, the minimum bandwidth is easy to compute. Since the execution time depends on actual input data, the maximum bandwidth cannot be computed. However, we can count the number of swaps $N_{swaps} \le N_{compex}$ performed to sort a given data file. Given $N_{swaps}$ we can compute the total number of bytes read and written as $16(N_{compex}+N_{swaps})$ and use it to compute the bandwidth for a given data file. Table \ref{tab:bandwidth} given $N_{swaps}$ and the bandwidth of all data files on a single 32 core place. The minimum bandwidth is 4.06 GB/s and the maximum is 4.89 GB/s, so far less than the total memory bandwidth of the system. Note that in all cases $N_{swaps}$ is far below the limit $N_{compex}$. Interestingly, the execution time to sort ur2.d is lower than r2.d, even though it requires more swaps. Most likely this is because the threads happen to be distributed in such a way that in the case of ur2.d more swaps are performed locally on a core, where memory consistency is strong. Note that none of the given data files come close to the worst case scenario, where $N_{swaps}=N_{compex}$. Since the behavior of the algorithm is well known, it may be possible to engineer a data file that does cause the worst case scenario for this specific problem.
\begin{figure}
\begin{center}$
\begin{array}{cc}
\includegraphics[scale=0.8]{../plots/dcachetotal_c1.png} &\\
\includegraphics[scale=0.8]{../plots/dcachetotal_r2.png}
\end{array}$
\caption{Total number of L1 D-cache hits/misses over time since simulator start. The upper graph shows the D-cache hits/misses of c1.d (data was already sorted), and the lower of r2.d (data is very unsorted).}
\label{fig:dcachetotal}
\end{center}
\end{figure}

\begin{table}
\begin{center}
\begin{tabular}{c || c | c}
& & Bandwidth\\
Data file & $N_{swaps}$ & (GB/s)\\\hline
c1 & 0 & 4.06\\
cm & 0 & 4.06\\
cz & 0 & 4.06\\
dec & 72704 & 4.38\\
inc & 0 & 4.06\\
r1 & 249242 & 4.86\\
r2 & 249412 & 4.86\\
ro1\_3l & 134016 & 4.50\\
ro1l & 114944 & 4.35\\
ro3\_5l\_1\_7r & 232020 & 4.85\\
topdec & 72704 & 4.38\\
topinc & 0 & 4.06\\
ur1 & 247807 & 4.86\\
ur2 & 252669 & 4.89\\
\end{tabular}
\end{center}
\caption{Memory bandwidth for the provided data files on a single 32 core place. Bandwidth = $16(N_{compex}+N_{swaps}) / \textrm{ execution time}$, where $N_{compex}=448511$ is the number of bytes read, $16N_{swaps}$ is the number of bytes written, and execution time is taken from Table \ref{tab:time}.}
\label{tab:bandwidth}
\end{table}

\pagebreak
\subsection{Family and thread allocations}
Figure \ref{fig:alloc} given an overview of the total number of families and threads allocated at a given point during execution of the program. The shape of the graphs is identical for any array of the same size, but as explained the execution time depends on the actual data. The number of allocated families remains roughly constant of most of the execution, except initially when many small families are created, and near the end when the final merge takes place.

\subsection{Pipeline efficiency}
Figure \ref{fig:pleff} shows the average pipeline efficiency for sorting c1.d and r2.d. The pipeline efficiency fluctuates most at the same time many threads are allocated (see Figure \ref{fig:pleff}). We believe this is because when many threads are active, there is a higher chance of cache misses, causing pipeline stalls. For r2.d (and other unsorted data files), there is also a high chance of branch misprediction in the compex operation. Because the data is unsorted the branch target cannot be predicted with accuracy. However, for c1.d this is not the case. Since the data is already sorted the branch target is always the same, and can be predicted with 100\% accuracy.


\begin{figure}
\begin{center}$
\begin{array}{cc}
\includegraphics[scale=0.8]{../plots/allocf_r2.png} &\\
\includegraphics[scale=0.8]{../plots/alloct_r2.png}
\end{array}$
\caption{Total number of families and threads allocated over all participating cores when sorting an array of 10240 elements. The shape of the graphs is identical for all inputs of the same length. The graphs have been smoothed because there is a lot of variation in the data.}
\label{fig:alloc}
\end{center}
\end{figure}

\begin{figure}
\begin{center}$
\begin{array}{cc}
\includegraphics[scale=0.8]{../plots/pl_eff_c1.png} &\\
\includegraphics[scale=0.8]{../plots/pl_eff_r2.png}
\end{array}$
\caption{Average pipeline efficiency over time taken as the moving average of the last 100 samples for sorting an array of 10240 elements on a single 32 core place. The upper graphs show the efficiency of c1.d, and the lower of r2.d. The graphs have been smoothed because there is a lot of variation in the data.}
\label{fig:pleff}
\end{center}
\end{figure}

\pagebreak
\section{Conclusions}
We have shown how to generalize odd-even merge sort for any input size and how we implemented it for Microgrid. We found that the greatest problem with the recursive algorithm is that the concurrency tree is unbounded and each family is very small. Furthermore, most of the concurrency is implicit in the concurrency tree. This leads to the inability to control available resources, and can cause deadlocks easily. Our solution was to implement iterative odd-even merge sort instead, which has none of the mentioned problems. The iterative algorithm has a fixed depth concurrency tree and favors large families that can be sequentialized if necessary. Most importantly, the iterative implementation can expose \emph{all} concurrency inherent in the algorithm. It can also adapt to any amount of given resources, even just a single core, without causing deadlocks. The iterative implementation gains a speedup of around 10x in the best case, a single 32 core place, compared to the non-parallelized implementation. We believe other divide-and-conquer algorithms may also benefit from an iterative solution.
\\\\
We have also shown that, although (iterative) odd-even merge sort is data independent and will always perform the same steps regardless of the input length, there is still a measurable difference in execution time depending on how sorted the input data actually is. The act of sorting means that data must be written to memory, which requires the memory coherency protocol to kick in to maintain consistency between cores, causing cache misses. In addition, if the input data is unsorted there will be branch mispredictions. Both of these affect measurably affect the execution time.
\\\\
Finally, we found that the results were not always sorted, depending on the Microgrid parameters (number of cores/places). We believe this might be a bug in the simulator, somewhere.

%\begin{thebibliography}{9}
%\end{thebibliography}
\addcontentsline{toc}{chapter}{Bibliography}
\bibliography{report}
\bibliographystyle{plainnat}
\end{document}