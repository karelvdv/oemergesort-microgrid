/*************************************
 *	Concurrent Systems Assignment
 *	Karel van der Veldt (5943086)
 *************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <svp/slr.h>
#include <svp/sep.h>
#include <svp/perf.h>
#include <svp/fibre.h>
#include <svp/delegate.h>

#ifdef BOTTOMUP_DEFAULT
#	define BOTTOMUP_PLACE PLACE_DEFAULT
#else
#	define BOTTOMUP_PLACE PLACE_LOCAL
#endif

#ifdef BITS64
typedef uint64_t sort_t;
#else
typedef uint32_t sort_t;
#endif

#define L1CACHE 1024

#ifdef COUNT_WRITES
static int nwrites=0;
#endif

/**
 *	Compare-exchange a[i] with a[j]
 */
static inline void compex(sort_t* a, uint64_t i, uint64_t j)
{
	if(a[i] > a[j])
	{
#ifdef COUNT_WRITES
		nwrites++;
#endif
		sort_t b = a[i];
		a[i] = a[j];
		a[j] = b;
	}
}

/**
 *	Computes log2(n) for n >= 1.
 */
static inline uint64_t log_2(uint64_t n)
{
	return 64 - __builtin_clzl(n) - !(n & (n - 1));
}

/**
 *	Rounds n to the smallest power of two >= n.
 *	n >= 1
 */
static inline uint64_t round2(uint64_t n)
{
#if 1
    return 1UL << log_2(n);
#else
#if 0 // naive
	uint64_t k = 1;
	while(k < n) k <<= 1;
	return k;
#else // bit twiddling hack
	n--;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n |= n >> 32;
	return n + 1;
#endif
#endif
}



/**********************************************
 *	Sequential Odd-Even Mergesort (Recursive)
 *	Time complexity:  O(nlogn)
 *	Space complexity: O(1)
 **********************************************/
#if 0
void seq_oemerge(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n, uint64_t step)
{
	if(n == 2)
	{
		compex(a, lo, lo + step);
	}
	else if(n == 3) // shortcut, not required for algorithm
	{
		uint64_t step2 = step << 1;
		compex(a, lo, lo + step);
		compex(a, lo, lo + step2);
		compex(a, lo + step, lo + step2);
	}
	else //if(n > 1), can be left out because of the above special case
	{
		uint64_t     m = n >> 1;
		uint64_t step2 = step << 1;
		seq_oemerge(a, lo, hi, n - m, step2);
		seq_oemerge(a, lo + step, hi, m, step2);

		for(uint64_t i = lo + step; i < hi - step; i += step2) compex(a, i, i + step);
	}
}

void seq_oemergesort(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n)
{
	if(n == 2) // shortcut, not required for algorithm
	{
		compex(a, lo, lo + 1);
	}
	else if(n == 3) // shortcut, not required for algorithm
	{
		compex(a, lo, lo + 1);
		compex(a, lo, lo + 2);
		compex(a, lo + 1, lo + 2);
	}
	else if(n > 1)
	{
		uint64_t m = round2(n) >> 1;
		seq_oemergesort(a, lo, lo + m, m);
		seq_oemergesort(a, lo + m, hi, n - m);
		seq_oemerge(a, lo, hi, n, 1);
	}
}
#endif

/**********************************************
 *	Parallel Odd-Even Mergesort (Recursive)
 *	Time complexity:  O(nlogn)
 *	Space complexity: O(1)
 **********************************************/

sl_def(par_compex,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, step) )
{
	sl_index(i);
	sort_t*   a = sl_getp(a);
	uint64_t step = sl_getp(step);
	compex(a, i, i + step);
}
sl_enddef


void par_oemerge(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n, uint64_t step, sl_place_t where);

sl_def(par_oemerge_two,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, lo0),
	sl_glparm(uint64_t, n0),
	sl_glparm(uint64_t, lo1),
	sl_glparm(uint64_t, n1),
	sl_glparm(uint64_t, hi),
	sl_glparm(uint64_t, step2),
	sl_glparm(sl_place_t, where),
)
{
	sl_index(i);
	sort_t* a = sl_getp(a);
	uint64_t step2 = sl_getp(step2);
	sl_place_t where = sl_getp(where);
	uint64_t hi = sl_getp(hi);
	uint64_t lo, n;
	
	if(i == 0)
	{
		lo = sl_getp(lo0);
		n  = sl_getp(n0);
	}
	else
	{
		lo = sl_getp(lo1);
		n  = sl_getp(n1);
	}
	
	par_oemerge(a, lo, hi, n, step2, where);
}
sl_enddef

void par_oemerge(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n, uint64_t step, sl_place_t where)
{
	if(n == 2)
	{
		compex(a, lo, lo + step);
	}
	else if(n == 3)
	{
		uint64_t step2 = step << 1;
		compex(a, lo, lo + step);
		compex(a, lo, lo + step2);
		compex(a, lo + step, lo + step2);
	}
	else
	{
		uint64_t     m = n >> 1;
		uint64_t step2 = step << 1;

#if 1
		par_oemerge(a, lo, hi, n - m, step2, where);
		par_oemerge(a, lo + step, hi, m, step2, where);
#else
		sl_create(, where, 0, 2, 1,,,
			par_oemerge_two,
			sl_glarg(sort_t*,, a),
			sl_glarg(uint64_t,, lo),
			sl_glarg(uint64_t,, n - m),
			sl_glarg(uint64_t,, lo + step),
			sl_glarg(uint64_t,, n),
			sl_glarg(uint64_t,, hi),
			sl_glarg(uint64_t,, step2),
			sl_glarg(sl_place_t,, where),
		);
		sl_sync();
#endif

#if 0
		sl_create(, where, lo + step, hi - step, step2,, sl__forceseq,
			par_compex,
			sl_glarg(sort_t*,, a),
			sl_glarg(uint64_t,, step)
		);
		sl_sync();
#else
		for(uint64_t i = lo + step; i < hi - step; i += step2) compex(a, i, i + step);
#endif
	}
}

sl_def(par_oemergesort,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, lo),
	sl_glparm(uint64_t, mi),
	sl_glparm(uint64_t, nm),
	sl_glparm(sl_place_t*, places),
	)
{
	sl_index(i);
	sort_t*   a = sl_getp(a);
	uint64_t lo, hi, n;
	sl_place_t* places = sl_getp(places);
	
	if(i == 0)
	{
		lo = sl_getp(lo);
		n  = sl_getp(mi);
		hi = lo + n;
	}
	else
	{
		lo = sl_getp(lo) + sl_getp(mi);
		n  = sl_getp(nm);
		hi = lo + n;
	}
	
	if(n > 1)
	{
		uint64_t m = round2(n) >> 1;

		sl_create(, places[i], 0, 2, 1,,,
			par_oemergesort,
			sl_glarg(sort_t*,, a),
			sl_glarg(uint64_t,, lo),
			sl_glarg(uint64_t,, m),
			sl_glarg(uint64_t,, n - m),
			sl_glarg(sl_place_t*,, places)
		);
		sl_sync();
		
		par_oemerge(a, lo, hi, n, 1, places[i]);
	}
}
sl_enddef

void do_par_oemergesort(sort_t* a, uint64_t len, sl_place_t* places)
{
	sl_create(,,,,,,,
		par_oemergesort,
		sl_glarg(sort_t*,, a),
		sl_glarg(uint64_t,, 0),
		sl_glarg(uint64_t,, len),
		sl_glarg(uint64_t,, 0),
		sl_glarg(sl_place_t*,, places)
	);
	sl_sync();
}
















/**********************************************
 *	Sequential Odd-Even Mergesort (Bottom-up)
 *	Time complexity:  O(nlogn)
 *	Space complexity: O(1)
 **********************************************/

/**
 *	Sequential bottom-up odd-even merge for powers of two only
 */
void seq_oemerge_bottomup_po2(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n)
{
#if 0
	uint64_t half = n / 2;
	for(uint64_t step2 = half; step2; step2 >>= 1)
	{
		uint64_t step4 = step2 << 1;
		for(uint64_t step = step2 & (half - 1); step < n - step2; step += step4)
		{
			for(uint64_t i = 0; i < step2; i++)
			{
				compex(a, lo + i + step, lo + i + step + step2);
			}
		}
	}
#else
	uint64_t half = n >> 1;
	
	for(uint64_t step = half, lg2 = log_2(half); step; step >>= 1, lg2--)
	{
		uint64_t offset  = step & (half - 1);
		uint64_t nchunks = (n - offset) >> (lg2 + 1);
		uint64_t start   = lo + offset;
		
		for(uint64_t i = 0; i < nchunks * step; ++i)
		{
			uint64_t chunk = i >> lg2;
			uint64_t idx   = i & (step - 1);
			uint64_t loc   = start + idx + (chunk << (lg2 + 1));
			compex(a, loc, loc + step);
		}
	}
#endif
}



/**
 *	Sequential bottom-up odd-even merge for any n
 */
void seq_oemerge_bottomup(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n)
{
	uint64_t half = round2(n) >> 1;
	
	for(uint64_t step = half, lg2 = log_2(half); step; step >>= 1, lg2--)
	{
		uint64_t step2   = step << 1;
		uint64_t offset  = step & (half - 1);
		uint64_t start   = lo + offset;
		uint64_t range   = hi - step - start;
		uint64_t nchunks = range >> (lg2 + 1);
		uint64_t nleft   = range & (step2 - 1);
		
		if(nleft > step) nleft = step;
		
		for(uint64_t i = 0; i < (nchunks << lg2) + nleft; ++i)
		{
			uint64_t chunk = i >> lg2;
			uint64_t idx   = i & (step - 1);
			uint64_t loc   = start + idx + (chunk << (lg2 + 1));
			compex(a, loc, loc + step);
		}
	}
}

void seq_oemergesort_bottomup_inner(sort_t* a, uint64_t n, uint64_t stride)
{
	uint64_t half = stride >> 1;
	
	for(uint64_t step = half, lg2 = log_2(half); step; step >>= 1, lg2--)
	{
		uint64_t offset  = step & (half - 1);
		uint64_t nchunks = (stride - offset) >> (lg2 + 1);
		
		for(uint64_t lo = 0; lo < n - stride + 1; lo += stride)
		{
			uint64_t start = lo + offset;
			
			for(uint64_t i = 0; i < nchunks * step; ++i)
			{
				uint64_t chunk = i >> lg2;
				uint64_t idx   = i & (step - 1);
				uint64_t loc   = start + idx + (chunk << (lg2 + 1));
				compex(a, loc, loc + step);
			}
		}
	}
}

/**
 *	Sequential bottom-up odd-even merge sort
 */
void seq_oemergesort_bottomup(sort_t* a, uint64_t n)
{
	for(uint64_t stride = 2; stride < n; stride <<= 1)
	{
		seq_oemergesort_bottomup_inner(a, n, stride);
		
		uint64_t rest = n & (stride - 1);
		// do the rest if it is not zero or a power of two
		// if it is a power of two then it was already sorted in the last iteration
		if( rest & (rest - 1) )
		{
			seq_oemerge_bottomup(a, n - rest, n, stride);
		}
	}
	
	seq_oemerge_bottomup(a, 0, n, n);
}



/**********************************************
 *	Parallel Odd-Even Mergesort (Bottom up)
 *	Time complexity:  O(nlogn)
 *	Space complexity: O(1)
 **********************************************/

sl_def(par_oemerge_bottomup,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, start),
	sl_glparm(uint64_t, step),
	sl_glparm(uint64_t, lg2)
)
{
	sl_index(i);
	sort_t* a      = sl_getp(a);
	uint64_t start = sl_getp(start);
	uint64_t step  = sl_getp(step);
	uint64_t lg2   = sl_getp(lg2);
	uint64_t chunk = i >> lg2;
	uint64_t idx   = i & (step - 1);
	uint64_t loc   = start + idx + (chunk << (lg2 + 1));
	compex(a, loc, loc + step);
}
sl_enddef

void do_par_oemerge_bottomup(sort_t* a, uint64_t lo, uint64_t hi, uint64_t n, sl_place_t* places)
{
	uint64_t half = round2(n) >> 1;
	
	for(uint64_t step = half, lg2 = log_2(half); step; step >>= 1, lg2--)
	{
		uint64_t step2   = step << 1;
		uint64_t offset  = step & (half - 1);
		uint64_t start   = lo + offset;
		uint64_t range   = hi - step - start;
		uint64_t nchunks = range >> (lg2 + 1);
		uint64_t nleft   = range & (step2 - 1);
		
		if(nleft > step) nleft = step;
		
		sl_create(, places[0], 0, (nchunks << lg2) + nleft, 1,,,
			par_oemerge_bottomup,
			sl_glarg(sort_t*,, a),
			sl_glarg(uint64_t,, start),
			sl_glarg(uint64_t,, step),
			sl_glarg(uint64_t,, lg2)
		);
		sl_sync();
	}
}

sl_def(par_oemerge_bottomup_inner,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, offset),
	sl_glparm(uint64_t, nchunks),
	sl_glparm(uint64_t, step),
	sl_glparm(uint64_t, lg2),
)
{
	sl_index(lo);
	sort_t* a        = sl_getp(a);
	uint64_t offset  = sl_getp(offset);
	uint64_t nchunks = sl_getp(nchunks);
	uint64_t step    = sl_getp(step);
	uint64_t lg2     = sl_getp(lg2);
	uint64_t start   = lo + offset;
	
	// PLACE_DEFAULT causes memory consistency problems here
	sl_create(, BOTTOMUP_PLACE, 0, nchunks * step, 1,,,
		par_oemerge_bottomup,
		sl_glarg(sort_t*,, a),
		sl_glarg(uint64_t,, start),
		sl_glarg(uint64_t,, step),
		sl_glarg(uint64_t,, lg2),
	);
	sl_sync();
}
sl_enddef


#ifdef TWO_PLACES
sl_def(par_oemerge_bottomup_two,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, offset),
	sl_glparm(uint64_t, nchunks),
	sl_glparm(uint64_t, step),
	sl_glparm(uint64_t, lg2),
	sl_glparm(uint64_t, n),
	sl_glparm(uint64_t, stride),
	sl_glparm(sl_place_t*, places),
)
{
	sl_index(i);
	uint64_t stride = sl_getp(stride);
	uint64_t n      = sl_getp(n);
	uint64_t total  = (n - stride) / stride;
	uint64_t split  = total / 2;
	uint64_t start, end;
	
	if(i == 0)
	{
		start = 0;
		end   = split * stride;
	}
	else
	{
		start = split * stride;
		end   = n - stride + 1;
	}
	
	sl_create(, sl_getp(places)[i], start, end, stride,,,
		par_oemerge_bottomup_inner,
		sl_glarg(sort_t*,, sl_getp(a)),
		sl_glarg(uint64_t,, sl_getp(offset)),
		sl_glarg(uint64_t,, sl_getp(nchunks)),
		sl_glarg(uint64_t,, sl_getp(step)),
		sl_glarg(uint64_t,, sl_getp(lg2)),
	);
	sl_sync();
}
sl_enddef
#endif

void do_par_oemergesort_bottomup_inner(sort_t* a, uint64_t n, uint64_t stride, sl_place_t* places)
{
	uint64_t half = stride >> 1;
	
	for(uint64_t step = half, lg2 = log_2(half); step; step >>= 1, lg2--)
	{
		uint64_t offset  = step & (half - 1);
		uint64_t nchunks = (stride - offset) >> (lg2 + 1);
		// divide computation over two places
		// first half of work goes to places[0], second half goes to places[1]
#ifdef TWO_PLACES
		sl_create(, PLACE_DEFAULT, 0, 2, 1,,,
			par_oemerge_bottomup_two,
			sl_glarg(sort_t*,, a),
			sl_glarg(uint64_t,, offset),
			sl_glarg(uint64_t,, nchunks),
			sl_glarg(uint64_t,, step),
			sl_glarg(uint64_t,, lg2),
			sl_glarg(uint64_t,, n),
			sl_glarg(uint64_t,, stride),
			sl_glarg(sl_place_t*,, places),
		);
		sl_sync();
		// do all computation on one place
#else
		sl_create(, places[0], 0, n - stride + 1, stride,,,
			par_oemerge_bottomup_inner,
			sl_glarg(sort_t*,, a),
			sl_glarg(uint64_t,, offset),
			sl_glarg(uint64_t,, nchunks),
			sl_glarg(uint64_t,, step),
			sl_glarg(uint64_t,, lg2),
		);
		sl_sync();
#endif
	}
}

#ifdef SPAWN_REST
sl_def(par_oemerge_bottomup_rest,,
	sl_glparm(sort_t*, a),
	sl_glparm(uint64_t, lo),
	sl_glparm(uint64_t, hi),
	sl_glparm(uint64_t, stride),
	sl_glparm(sl_place_t*, places),
)
{
	do_par_oemerge_bottomup(sl_getp(a), sl_getp(lo), sl_getp(hi), sl_getp(stride), sl_getp(places));
}
sl_enddef
#endif

void do_par_oemergesort_bottomup(sort_t* a, uint64_t n, sl_place_t* places)
{
	for(uint64_t stride = 2; stride < n; stride <<= 1)
	{
#ifndef SPAWN_REST
		do_par_oemergesort_bottomup_inner(a, n, stride, places);
		
		uint64_t rest = n & (stride - 1);
		if( rest & (rest - 1) )
		{
			do_par_oemerge_bottomup(a, n - rest, n, stride, places);
		}
#else
		sl_spawndecl(therest);
		uint64_t rest = n & (stride - 1);
		if( rest & (rest - 1) )
		{
			sl_spawn(therest,,,,,,,
				par_oemerge_bottomup_rest,
				sl_glarg(sort_t*,, a),
				sl_glarg(uint64_t,, n - rest),
				sl_glarg(uint64_t,, n),
				sl_glarg(uint64_t,, stride),
				sl_glarg(sl_place_t*,, places),
			);
		}
		
		do_par_oemergesort_bottomup_inner(a, n, stride, places);
		
		if( rest & (rest - 1) ) sl_spawnsync(therest);
#endif
	}
	
	do_par_oemerge_bottomup(a, 0, n, n, places);
}






/**
 *	Parallel verify if a is sorted.
 */
sl_def(par_verify,,
	sl_glparm(sort_t*, a),
	sl_shparm(uint64_t, yes)
	)
{
	sl_index(i);
	sort_t*  a = sl_getp(a);
	uint64_t cmp = a[i - 1] <= a[i];
	sl_setp(yes, sl_getp(yes) + cmp);
}
sl_enddef

/**
 *	Verify if a is sorted.
 */
int64_t verify(sort_t* a, uint64_t len)
{
// in terms of cycles a sequential loop is slower, but in terms of wall clock time it is faster!
#if 1
	for(int64_t i = 1; i < len; ++i) if(a[i - 1] > a[i]) return 0;
	return 1;
#else
	sl_create(, PLACE_DEFAULT, 1, len, 1,,,
		par_verify,
		sl_glarg(sort_t*,, a),
		sl_sharg(uint64_t, yes, 1)
	);
	sl_sync();
	return sl_geta(yes) == len;
#endif
}




slr_decl(
	slr_var(int, cores, "number of cores"),
	slr_var(int, print, "print time and verify"),
);

/**
 *	Main
 */
int main(int argc, char** argv)
{
	counter_t ct[2][MTPERF_NCOUNTERS];
	sl_place_t places[2] = {PLACE_DEFAULT, PLACE_DEFAULT};
	clock_t clocks = 0;
	sort_t* a      = NULL;
	uint64_t len   = 0;
	int cores      = slr_get(cores)[0];
	
	sep_alloc(root_sep, places + 0, SAL_MIN, cores);
#ifdef TWO_PLACES
	sep_alloc(root_sep, places + 1, SAL_MIN, cores);
#else
	places[1] = places[0];
#endif
	if(places[0] == -1 || places[1] == -1)
	{
		printf("SEP allocation failed!!!\n");
		return 1;
	}
	
	if( fibre_tag(0) != -1 )
	{
		len = fibre_shape(0)[0];
		a = (sort_t*)fibre_data(0);
	}
	else
	{
		len = 10240;
		a = malloc(sizeof(sort_t) * len);
		for(int i = 0; i < len; ++i) a[i] = len - i;
	}
	
	mtperf_sample(ct[0]);
#ifdef RECURSIVE
	do_par_oemergesort(a, len, places);
#else
	//seq_oemergesort_bottomup(a, len);
	do_par_oemergesort_bottomup(a, len, places);
#endif
	mtperf_sample(ct[1]);
	
	clocks = ct[1][MTPERF_CLOCKS] - ct[0][MTPERF_CLOCKS];
	
	if( slr_get(print)[0] )
	{
		printf("%d;%lu;%lu;%lu\n", cores, clocks, clocks / (CLOCKS_PER_SEC / 1000000), verify(a, len) );
#ifdef COUNT_WRITES
		printf("writes: %d\n", nwrites);
#endif
	}
	return 0;
}

