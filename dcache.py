iprev  = zerorec
hits   = select('*.dcache.numHits')
misses = select('*.dcache.numMisses')
for i in input:
	allhits   = sum(i[x] for x in hits)
	allmisses = sum(i[x] for x in misses)
	dhits     = sum(i[x] - iprev[x] for x in hits)
	dmisses   = sum(i[x] - iprev[x] for x in misses)
	dt = i[kernel.cycle] - iprev[kernel.cycle]
	tabulate(i[kernel.cycle], allhits, allmisses, dhits / dt, dmisses / dt)
	iprev     = i
