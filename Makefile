CC=slc
CARGS=-b mta -Wall -O1 -DBITS64
SRC=mgsort.c

all: ${SRC}
	${CC} ${CARGS} ${SRC} -o mgsort

run: all
	slr -m mlcoma256_4chan -Ws,-q -n 4 mgsort cores=32
