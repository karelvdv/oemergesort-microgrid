# Rounds n to the smallest power-of-two >= n.
def round2(n):
	k = 1
	while k < n:
		k = k * 2
	return k

# Compare-exchanges a[i] and a[j].
# invariant: j > i
def compex(a, i, j):
	if a[i] > a[j]:
		a[i], a[j] = a[j], a[i]

# Merges the elements a[lo], a[lo+step], a[lo+2*step], ..., a[hi].
# n is the number of elements in the range a[lo..hi] to be merged.
def oemerge(a, lo, hi, n, step):
	if n == 2:
		compex(a, lo, lo + step)
-	else:
+	elif n > 1:
		m     = n / 2
		step2 = step * 2
		# Both interleaved arrays could be parallelized,
		# but it causes memory consistency problems.
-		oemerge(a, lo, hi, m, step2)
+		oemerge(a, lo, hi, n - m, step2)
		oemerge(a, lo + step, hi, m, step2)
		# sl_create(, PLACE_LOCAL, ...)
		for i in xrange(lo + step, hi - step, step2):
			compex(a, i, i + step) # Never overlaps.

# Sorts the elements a[lo..hi], n = hi - lo.
# To sort an array a of length n, call oemergesort(a, 0, n, n).
def oemergesort(a, lo, hi, n):
	if n > 1:
-		m = n / 2
+		m = round2(n) / 2
		# Both calls to oemergesort can be parallelized,
		# which requires a family of two threads.
		# sl_create(, some place, 0, 2, ...)
		oemergesort(a, lo, lo + m, m)
-		oemergesort(a, lo + m, hi, m)
+		oemergesort(a, lo + m, hi, n - m)
		oemerge(a, lo, hi, n, 1)